{ config, pkgs, lib, ... }:
let
  home = config.home.homeDirectory;
in
{
  imports = [
    ./stuff/programs.nix ./stuff/xsession.nix
    ./stuff/secret.nix
    ./stuff/services.nix
  ];
  nixpkgs = {
    config = {
      allowUnfree = true;
    };
  };

  age = {
    sshKeyPaths = [ "${home}/.ssh/id_ed25519" ];
    secrets = {
      /*     fish_variables = {
        file = ./secrets/fish_variables.age;
        path = "${home}/.config/fish/fish_variables";
        mode = "660";
        };
      
       mpdasrc = {
       file = ./secrets/mpdasrc.age;
         path = "${home}/.config/mpdasrc";
       };
       */
      zshrc = {
      #  file = ./secrets/.zshrc.age;
        path = "${home}/.zshrc";
        mode = "660";
      };
    };
  };
  home = {
    packages = with pkgs; [
      ffmpeg-full
      gh
      curl 
      wget
      sox
      weechat
      gnumake
      gcc
      g++
      alacritty
      rage
      curl
      pamixer
      mpdas
      pavucontrol
      master.discord
      customscripts
      mpd_discord_richpresence
      qbittorrent
      luajit
      mpv
      jmtpfs
      dunst
      flameshot
      youtube-dl
      xclip
      xorg.xkbcomp
      xorg.xmodmap
      p7zip
      unrar
      glxinfo
      sxiv
      vim
      feh
      dmenu
      neofetch
      xmobar
      xdotool
      arc-theme
      arc-icon-theme
      wineWowPackages.stable
      master.winetricks
      games.winestreamproxy
      csvtool
      pmidi
      dosbox
      nbfc-linux
      pulseaudio
      (texlive.combine { inherit (texlive) scheme-small babel lm graphics-def url; })
      ardour
      carla
      anki-bin
      spotify
    ];

    file = {
      dwm-autostart = {
        source = ./config/dwm/autostart.sh;
        target = "${home}/.dwm/autostart.sh";
      };
      dwm-status = {
        source = ./config/dwm/bruhstatus.sh;
        target = "${home}/.dwm/bruhstatus.sh";
      };
      dunstrc = {
        source = ./config/dunst/dunstrc;
        target = "${home}/.config/dunst/dunstrc";
      };
    };
  };
}
