#867-43c8-b314-e254ce2ff77f Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "nvme" "xhci_pci" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ];
  boot.supportedFilesystems = [ "ntfs" "btrfs" ];


#  fileSystems."home/dumball/mnt/home4" =
#  { device = "/dev/disk/by-uuid/8e086312-a867-43c8-b314-e254ce2ff77f";
#    fsType = "btrfs";
#    options = ["rw" "uid=1000"  "compress-force=zstd:3" ];
#  };

  

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/a0848f1a-fc0c-424b-b9b9-e62ca8b61c14";
      fsType = "ext4";
    };

 fileSystems."/boot" =
   { device = "/dev/disk/by-uuid/4CF2-4701";
     fsType = "vfat";
   };
     
   fileSystems."/home2" =
    { device = "/dev/disk/by-uuid/B6CAFD08CAFCC5A1";
      fsType = "ntfs"; 
      options = [ "rw" "uid=1000"];
    };
    
    fileSystems."/mnt/home3" =
    { device = "/dev/disk/by-uuid/1A784CA0784C7C8F";
      fsType = "ntfs";
      options = [ "rwx" "uid=1000"];
    };  
    swapDevices = [ ];

      zramSwap = {
    enable = false;
    algorithm = "lz4";
    memoryPercent = 30;
    priority = -1;
  };

#    fileSystems."/boot/efi" =
#    {
#      device = "/dev/disk/by-uuid/4CF2-4701";
#      fsType = "vfat";
#    };

  hardware = {
    bluetooth.enable = true;
  };

}
