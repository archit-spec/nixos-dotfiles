{config, lib, pkgs, ...}: {


  services =  {

    openssh = {
      enable = true;
       permitRootLogin = "yes";
      ports = [ 22001 22002 ];
    };
    nginx  = {
    enable = true;
    package = (pkgs.nginx.overrideAttrs (oa: {
    configureFlags = oa.configureFlags ++ [ "--with-mail" "--with-mail_ssl_module" ];
     }));
     virtualHosts = 
      let 
         genericHttpRProxy = { addr, ssl ? true, conf ? "" }: {
            addSSL = true;
            enableACME = ssl;
            locations."/" = {
              proxyPass = toString addr;
              extraConfig = ''
                proxy_set_header Host $host;
              '' + conf;
            };
          };
        in
        {

     "radio.dumball.in" = genericHttpRProxy{addr = "10.0.0.146:6600";};
     "colab.dumball.in" = genericHttpRProxy{addr = "10.0.0.146:8080";};
    };
       "dumball.in" = {
         addSSL = true;
         enableACME = true;
         locations."/".proxyPass = "http://10.0.0.146:80";
         serverAliases = [ "www.dumball.in" ];
        };


        users.users.root.openssh.authorizedKeys.keys = [

        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHMFBOAp+J5q77ytUfqRfhTh0ksDTo5S4dYCv+SHAkOh archit1290@gmail.com"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC03Sv4J+2nMxFRpH5pUWN2JTCiuLHg+duNIqsKX9FTBtJ8fOUjth0aE+XEKrUq+FRq8dVcYk0qMiZ+WlQaVcEoloyBpw7YaLRj42+T9irKNpDZbAM1frTxnQiycRqqn000IOZFxL2iew6uOFL5j79J0gsUeNzy9ncIBXkVlT02GMMLCzBEjyAKgmhiCy9seMi68ZsiXyyxez2ocbKNHs6WphGwl1AmhxD0uisnzJqnlwgfrqVerOlp50j8Do0caLtovmC4Fruha5AmIgzHPlly95EYM1gpwO05ynOqxzAxPeCjfowJSqKpfQMAqBagpui2JE3E1ZlRGd0VMfnF/Zy9g0bSlRw7WTh3kS9uLNLDqkVgQxLuJASCeNbyvwkYsQAAUY/NMec3aOU+AThDtRW8ySKRKXaS9lksA/Al6wbGLKETiiRcQgtpdrPbRPTCmRdpW/NJo1VFoWokpDkP6eLPaw5clsENPCUzE5BmASpZApi9hnvrby7O/RLE92Szk9U= archit1290@gmail.com"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCc/Lq1on30sN6KAhrWOg0ZAOAbUlXCkBYFDlTjvCq90d4lEjKbbTnd6Y7OZIwRqLsc5D9cSxfzi79lliq8+3cO5CtLwHMw9rxHiHrDb9OowSitd2P0oW40sGafSmbK3wzq+zFT4fv4rZK7wjIeD9zAw3ftnI1wI8onEDvMW0ZySURCKK1GEcD5reKR05W9tAmyQZujpOVE4wZXLd6vXc7u+oeeoH/qNFYgO3BX4GBr+MhFo2wgr/6WnivTxV0wf/cIUQMlmNAMB1FqBV52svHDxBKLPWNE84+18FiRMI3FVdbGPGKIBBgR7UfexCQE9p2i7fFINTXY93ezETSX7iuWUnrfAhTg7K8db0KhVFkXFby7SZc6+4x/oMhhhFXLoy1WttOCdG7oWl9qhzqp4JCj6xDbbITEFggBrEoGo8rNYBeEr+2s0w6BZCxJIvgNUN46Kkciqdp8mZY45RvIA26P849uNeht9X9phTHo8ZlKrjYsL+wKaVZV5vVXV31qNR8= dumball@dumball"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIC7GXf/POyUU2ICwlPu84tVqaKMIbuTyuuYMqih5Z03 archit1290@gmail.com"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC1OIwXKTHpBZakGA28suW1W+47ZhdXOJ871zeqk0XAujeyrS9jVTL0TX8S6Noj38nWr4gR3nC/Ja8bEeKP0Kh/afaKHXn0KNfH4tEhL2+sSKksSfsc0YhXfbXuDYV+eHVAzcvcbuvRqjTgC0yb0qDGDFyLjkiPiU0u4yH0OmSo6afyczYkNezABtWvuGk8u4OZc2uhwIfNvpUsI/yYXD/zrqz9hFcWBGQNHQQv9qBEccoJFG9KskqUdeYpzpcSB4Bf4zIhlD3xoHqfzgNRQKHGka/cVoKhNzn2f/Ke5m+UYDpEKbLOJFBeLvOpdujqJTJVLElt9b6IcGth//vLEP5phcy6vFrLR3zCXIYcVwke4dBQNdvXEEsghR8W3pp6pXjD6U5ikawcW5gwJJOMkCfmsSqOsyM8DdbuNg2wWD6KChWWKtzZ90WtVefR1KrWLTipdkYFZqvjNtcW3pGhycqZkgXxDuuU8ncsjrE+yoFAeVjsCl8R+LopszFuGaGDDt0= dumball@dumball"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID9j1HuzRt6dx1oNWViSy5C46znZii1l+ufD2DeAUzL8 archit1290@gmail.com"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC32Darsy2JNZb0Y1clh6zCaE7WWxYyM0muxi/FvOOCfVMkQ2fcSNQXtpfx3Am93mTuRM/sx2OQwSrNUGGumPJrU/n+BwC0rKJD30mtHEcvySkt+6eblt8rYTpxyXIsmkxBGFXnn7/6qQOLpeGSQOPGRyYzhkY2boEi78LNFAxKmgqCyCVDSFdfTv1aV9II9fnZFHLhFg2XEBIPGRkTMTThMHJ+4S0gSQCOOqccGQ5CpGfwdJFyzOCYXY9kzgLFuDCU+yEC/PYPqWvDlAapE7zSsIbrWcZ0UBZ6kKcMlUvEDPHWyLzSYtrPfIx5tbGv9r2G8PNfoD83vnL1gUyrYjqp ssh-key-2022-04-17"

      ];

      security.acme = {
    acceptTerms = true;
    certs = {
      "dumball.in".extraDomainNames = lib.singleton "www.dumball.in";
    } //
    lib.mapAttrs' (n: _: lib.nameValuePair n ({ email = "archit@dumball.in"; })) config.services.nginx.virtualHosts;
  };
  security.pki.certificateFiles = [ ../../cert.pem ];
      
      };
    };
  }
      








