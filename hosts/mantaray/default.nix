{config, lib, ...}:
{

  import = [
    ./hardware.nix
    ./services.nix

  ];
}
