{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/profiles/qemu-guest.nix")
    ];

    fileSystems."/" =
    { device = "/dev/disk/by-uuid/229dc4c2-8b6b-41f0-a12f-d3299d67d7ff";
      fsType = "ext4";
    };


  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.kernelParams = [
    "console=ttyS0"
    "console=tty1"
    "nvme.shutdown_timeout=10"
    "libiscsi.debug_libiscsi_eh=1"
  ];

  boot.initrd.kernelModules = [ "bochs_drm" ];

  swapDevices = [
    {
      device = "/swapfile";
      priority = 0;
    }
  ];

}
