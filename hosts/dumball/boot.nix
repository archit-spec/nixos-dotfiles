{lib, config, pkgs, ... }:

{
 nixpkgs.config.allowUnfree = true; 
  boot = {
#     kernelPackages = pkgs.unstable.linuxPackages_zen;
     kernelPackages = pkgs.linuxPackages_latest;

    initrd={
#      availableKernelModules = [ "xhci_pci" "ahci" "nvme" "usb_storage" "sd_mod" "vfio-pci"];
 /*     preDeviceCommands = ''
        DEVS="0000:01:00.0 0000:01:00.1 0000:01:00.2 0000:01:00.3"
        for DEV in $DEVS; do
          echo "vfio-pci" > /sys/bus/pci/devices/$DEV/driver_override
        done
        modprobe -i vfio-pci
        '';*/
      kernelModules = ["amdgpu" ];
    };
    kernelModules = [  "nvidia-uvm" "kvm-amd"];
    extraModulePackages = with config.boot.kernelPackages; [ nvidia_x11 ];
    loader = {
      efi = {
     canTouchEfiVariables = true;
       efiSysMountPoint = "/boot/efi";
      };
      grub = {
#       efiInstallAsRemovable = true;
        enable = true;
        useOSProber = true;
        efiSupport =  true;
        device = "nodev";
        splashImage = ./alo.png;
        splashMode = "stretch";
 #      configurationName = "nixbruh";
      };
    };
 #   binfmt.emulatedSystems = [ "aarch64-linux" ];
  };
}
