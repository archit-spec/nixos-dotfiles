{lib, config, ...}:
{
  imports = [
    ./alo.nix
    ./blender.nix
    ./boot.nix
    ./default.nix
    ./nvdia.nix
    ./stuff.nix
  ];

 system.stateVersion = "21.05";
}
