{ config, pkgs, ... }:
{
 networking.enableIPv6 = false;
 users.extraUsers.dumball.extraGroups = ["wheel"];
 networking =  {
   hostName = "dumball"; # Define your hostname.
   wireless.enable = true;# Enables wireless support via wpa_supplicant.
   wireless.userControlled.enable = true; 
   wireless.interfaces = ["wlp4s0"];

   wireless.networks.Akshat.pskRaw = "4fb4def18646584f6e6bea59c72173c392c4b09faf71e5d1c1e1ebafb6e8bb82";
  wireless.networks.dumball.pskRaw = "d9e4ea19117cb85d36cb2c92de3cca892adb767e3655c41075ea79b08ac8effb";
  #networking.networkmanager.enable = true;
extraHosts =
  ''
    122.183.45.124 dumball.in
  '';
   interfaces = {
   wlp4s0 = {
   useDHCP = true;
 #  ipv4.addresses = [{
 #         prefixLength = 24;
 #         address = "223.178.248.231";
  #   }];
   
     };

   };
   
 };


  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22 18172 6600 8000 5432 ];
   networking.firewall.allowedUDPPorts = [ 22 ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

}


