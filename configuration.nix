# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./stuff.nix
      ./networking.nix
      ./nvdia.nix
#      ./blender.nix
      ./alo.nix
#      ./hydra.nix
#     ./boot.nix
#     ./modules/xorg.nix
#  ./overlays/overridesandshit.nix
  ./nginx.nix
  ./kk.nix
#  ./mpd.nix
    ];
 


nixpkgs.config.allowBroken = true;

nixpkgs.config.allowUnfree = true;
virtualisation.libvirtd.enable = true;
programs.dconf.enable = true;

 #boot.loader.grub.devices = "nodev";
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;


  # Set your time zone.
   time.timeZone = "Asia/Kolkata";
  
  nix = {
    package = pkgs.nixUnstable; # or versioned attributes like nix_2_4
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
   };
###
services.xserver.videoDrivers = [ "nvidia" ];
hardware.opengl.enable = true;

###
  #for gpu  
 # nixpkgs.config.allowUnfree = true;
 # boot.extraModulePackages = [ pkgs.linuxPackages.nvidia_x11 ];
#  boot.blacklistedKernelModules = [ "nouveau" "nvidia_drm" "nvidia_modeset" "nvidia" ];
#  packages = [ pkgs.linuxPackages.nvidia_x11 ];




#nix.settings.auto-optimise-store = true;

nix.settings.auto-optimise-store = true;
nix.autoOptimiseStore = true;
  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";


  # Enable the X11 windowing system
  environment.loginShellInit = ''
    if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
      exec sway --unsupported-gpu
    fi
  '';
#  services.xserver.enable = true;
#  services.xserver.displayManager.startx.enable = true;
 #services.xserver.displayManager.lightdm.enable = true; 
#services.xserver.displayManager.gdm.enable = true;
  services.xserver.enable = true;
  #services.xserver.displayManager.defaultSession = "sway";
 programs.qt5ct.enable = true;
 services.xserver.displayManager.gdm.enable = true;
#  services.xserver.libinput.enable = true;
 programs.sway = {
   enable = true;
   wrapperFeatures.gtk = true;
   extraOptions = ["--unsupported-gpu" ];
   extraPackages = with pkgs; [
   swaylock
   swayidle
   wl-clipboard
   mako
   wofi
   rofi

   ];

 };
  services.xserver.windowManager = {
    dwm.enable = true;
    xmonad.enable = true;
    xmonad.enableContribAndExtras = true;
    xmonad.extraPackages = hpkgs: [
    hpkgs.xmonad
    hpkgs.xmonad-extras
];
};
  
  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
   sound.enable = true;
   hardware.pulseaudio.enable = true;

 #  services.xserver.libinput.enable = true;

 # Define a user account. Don't forget to set a password with ‘passwd’.
   users.users.dumball = {
     isNormalUser = true;
     extraGroups = [ "wheel" ]; 
   };

   environment.systemPackages = with pkgs; [
     blender
     vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
     virt-manager
     firefox
     glib
     wget 
     wirelesstools
     networkmanager
     xclip
     pciutils usbutils
     git
     neovim
     alacritty
     ##for xmonad
     picom
     scrot
     trayer
     emacs
     ntfs3g
     kbd
     ripgrep
     bc
     gnumake
     gcc
     bind
   ];


  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
 programs.mtr.enable = true;
# programs.sway.enable =true;
 programs.gnupg.agent = {
   enable = true;
   enableSSHSupport = true;
 };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
   services.openssh.enable = true;

  # Open ports in the firewall.
 #  networking.firewall.allowedTCPPorts = [ 6600 80 ];
 #  networking.firewall.allowedUDPPorts = [ 6600 80 ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

}

