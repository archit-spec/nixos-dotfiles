{
inputs = {
    stable.url = github:nixos/nixpkgs/nixos-21.11;
    old.url = github:nixos/nixpkgs/nixos-21.05;
    nixpkgs.url = github:nixos/nixpkgs/nixpkgs-unstable;
    master.url = github:nixos/nixpkgs/master;
    home-manager.url = github:nix-community/home-manager;
    home-manager-stable.url = github:nix-community/home-manager/release-21.11;
    nur.url = github:nix-community/NUR;
  #utils.url = github:numtide/flake-utils;
    utils.url = "github:gytis-ivaskevicius/flake-utils-plus";

    nvim.url = github:nix-community/neovim-nightly-overlay;
    mailserver.url = gitlab:simple-nixos-mailserver/nixos-mailserver;
    nbfc.url = github:nbfc-linux/nbfc-linux;
    emacs.url = github:nix-community/emacs-overlay;
    nix-gaming.url = github:fufexan/nix-gaming;
    rust.url = github:oxalica/rust-overlay;
  };
  outputs = {  self, nixpkgs, stable, master, old, utils,  ... }:
     let
        inherit (utils.lib) mkFlake exportModules;
        pkgs = self.pkgs.x86_64-linux.nixpkgs;
     in 
     mkFlake {
       inherit self inputs;
      
       supportedSystems = [ "x86_64-linux" ];
       channelsConfig


     }

  {
    nixosConfigurations.dumball = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [ ./configuration.nix
                  ];
    };
  };
}









