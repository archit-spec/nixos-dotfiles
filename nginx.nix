{config, pkgs, lib, ... }:{
services = {
nginx = {
      appendHttpConfig = "listen 127.0.0.1:80";  
      enable = true;
      package = (pkgs.nginx.overrideAttrs (oa: {
        configureFlags = oa.configureFlags ++ [ "--with-mail" "--with-mail_ssl_module" ];
      }));
      virtualHosts =
        let
          genericHttpRProxy = { addr,  conf ? "" }: {
     #       addSSL = true;
     #       enableACME = ssl;
            locations."/" = {
              proxyPass = toString addr;
              extraConfig = ''
                proxy_set_header Host $host;
              '' + conf;
            };
          };
        in
        {
          "dumball.in" = genericHttpRProxy { addr = "https://122.183.45.124:80"; 

        };
        };
      };
      
    };
  }
