{ config, pkgs, lib, ...}:
let
  cfg = config.dumball.services.mpd;
in
with lib; {
  options = {
    dumball.services.mpd = {
      enable = mkOption {
        default = false;
        type = with types; bool;
        description = ''
          Enable the music player daemon.
        '';
      };

      mpris = mkOption {
        default = false;
        type = with types; bool;
        description = ''
          Enable MPRIS support for MPD.
        '';
      };

      scrobbling = mkOption {
        default = false;
        type = with types; bool;
        description = ''
          Enable the MPDScribble audio scrobbler.
        '';
      };
    };
  };


  config = mkIf cfg.enable {
    # create a systemd service for mpdscribble
    # https://github.com/MusicPlayerDaemon/mpdscribble/blob/master/systemd/user/mpdscribble.service.in
    systemd.user.services.mpdscribble = mkIf cfg.scrobbling {
      enable = true;
      description = "Audio scrobbler for MPD";
      after = [ "mpd.service" ];

      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.mpdscribble}/bin/mpdscribble --no-daemon --conf /home/dumball/.config/mpdscribble/mpdscribble.conf";
        Restart = "on-failure";
      };

      wantedBy = [ "multi-user.target" ];
    };

    dumball.home = {
      services.mpd = {
        enable = true;
        musicDirectory = /home/dumball/usr/music;

        # enable pulse and ncmpcpp visualizer
        extraConfig = ''
	 input {
                plugin "curl"
        }
        audio_output {
                type "pulse"
                name "pulse audio"
        }
        audio_output {
                type "fifo"
                name "Visualizer feed"
                path "/tmp/g.fifo"
                format "44100:16:2"
        }
        audio_output {
                encoder     "vorbis"
                type        "httpd"
                quality     "9"
                name        "Vorbis Stream"
                port        "8000"
                max_clients "4"
        }
      '';
      };

      # the client
      home.file.".ncmpcpp/config".source = /etc/nixos/config/ncmpcpp/config;
      programs.zsh.shellAliases = { "m" = "ncmpcpp"; };

      # mpris support
      services.mpdris2 = {
      };

      # load the mpdscribble config. this is private.
      xdg.configFile."mpdscribble/mpdscribble.conf".source =
        /etc/nixos/config/mpdscribble/mpdscribble.conf;

      home.packages = with pkgs; [
        dumball.ncmpcppWithVisualizer
      ] ++
      (if cfg.mpris then [
        mpdris2
        playerctl
      ] else []) ++
      (if cfg.scrobbling then [
        mpdscribble
      ] else []);
    };
  };
}

