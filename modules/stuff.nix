{lib, config, agenix, pkgs, ...}:

{
 fonts.fonts = with pkgs; [
    fira-mono
    nerdfonts
    font-awesome
    noto-fonts-cjk
  ];

 users.users.dumball = {
    isNormalUser = true;
    shell = pkgs.fish;
    home = "/home/dumball";
    extraGroups = [ "wheel" "adbusers" "video" "libvirtd" "docker" ];
  };

  gtk.iconCache.enable = true;
  virtualisation.docker.enable = true;


}  
