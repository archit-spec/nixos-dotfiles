{ lib, config, pkgs, ... }:
let
  home = config.home.homeDirectory;
in
{
  environment.systemPackages = with pkgs; [
    git
    htop
    vim
    wireguard-tools
    vault
    tree-sitter
    rnix-lsp
    nmap
    gcc
    fly
    postgresql
  ];
 services = {
 postgresql = {
  enable = true;
  package = pkgs.postgresql_11;

  };
 mpd = {
   enable = true;
#     musicDirectory = "$home/Music";
#      dbFile = "${config.home.homeDirectory}/.config/mpd/database";
#      dataDir = "${config.home.homeDirectory}/.config/mpd";
      network = {
        listenAddress = "any";
        port = 6600;
      };

  extraConfig = ''
        input {
                plugin "curl"
        }
        audio_output {
                type "pulse"
                name "pulse audio"
        }
        audio_output {
                type "fifo"
                name "Visualizer feed"
                path "/tmp/g.fifo"
                format "44100:16:2"
        }
        audio_output {
                encoder     "vorbis"
                type        "httpd"
               quality     "9"
                name        "Vorbis Stream"
                port        "8000"
                max_clients "4"
        }
        filesystem_charset "UTF-8"

  '';

 };
# mopidy = {
#   enable = true;
#   extensionPackages = [ pkgs.mopidy-spotify ];
#   configuration  =  "
#    [soundcloud]
#auth_token = 3-35204-622741266-rMrzJn0wBJgQ4nh
#enabled = true
#username = archi1290@gmail.com
#password = A9752295907
#
#
#
#[#spotify]
#enabled =true
#username = archi1290@gmail.com
#password = tammu123
#[spotify]
#client_id = e41c52b3-d54b-4a05-bbfc-5b51ad6c5dca
#client_secret = XhR3v3gpEOuO7D3KBAi9ZDsI2ArrP3jQ92AYqRBYiMY=
#bitrate = 320
#cache_dir = $XDG_CACHE_DIR/mopidy/spotify
#};

 };
 users.defaultUserShell = pkgs.zsh;
 programs = {
    neovim = {
    enable = true;
    };
    zsh = {
     enable = true;
   # promptInit = "PROMPT='%B%F{cyan}%~ %F{blue}>%f%b '\nRPROMPT='%B%F{cyan}%n%f@%F{red}%m%b'";
     histSize = 12000;
    enableCompletion = true;
      syntaxHighlighting.enable = true;
      autosuggestions. enable = true;
        ohMyZsh = {
    enable = true;
    plugins = [ "git"];
    theme = "lambda";
  };
    };
    gnupg = {
      agent = { 
        enableSSHSupport = true;
        enable = true;
        pinentryFlavor = "curses";
      };
    };
  };
  virtualisation.virtualbox.host.enable = true;
  
}
