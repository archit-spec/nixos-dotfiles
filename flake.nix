{
  inputs = {
    stable.url = github:nixos/nixpkgs/nixos-21.11;
    old.url = github:nixos/nixpkgs/nixos-21.05;
    nixpkgs.url = github:nixos/nixpkgs/nixpkgs-unstable;
    master.url = github:nixos/nixpkgs/master;
    home-manager.url = github:nix-community/home-manager;
    home-manager-stable.url = github:nix-community/home-manager/release-21.11;
    fup.url = github:gytis-ivaskevicius/flake-utils-plus;
    nixpkgs-wayland = {
      url = github:colemickens/nixpkgs-wayland;
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.unstableSmall.follows = "nixpkgs";
    };
    nur.url = github:nix-community/NUR;
    utils.url = github:numtide/flake-utils;
    neovim.url = github:nix-community/neovim-nightly-overlay;
    mailserver.url = gitlab:simple-nixos-mailserver/nixos-mailserver;
    nbfc.url = github:nbfc-linux/nbfc-linux;
    emacs.url = github:nix-community/emacs-overlay;
    rust.url = github:oxalica/rust-overlay;
  };

  outputs = inputs@{fup, self, utils, nixpkgs, stable, master, old, nur, neovim, home-manager, home-manager-stable, ... }:
  let
    inherit (fup.lib) exportOverlays exportModules exportPackages;
     mkApp = utils.lib.mkApp;
     uites = import ./suites.nix { inherit utils; };

  in
   fup.lib.mkFlake{
   inherit self inputs;
   channelsConfig.allowUnfree = true;
    channels.unstable.overlaysBuilder = channels: [
    (final: prev: { inherit (channels.unstable) neovim; })
  ];
       sharedOverlays = [
       nur.overlay
       self.overlay
       # Use `neovim.packages.${system}.neovim`, for reproducibility with neovim's flake
       (utils.lib.genPkgOverlay neovim "neovim")
     ];
   hostDefaults.modules = [
      home-manager.nixosModules.home-manager
     ./modules
   ];
   hosts.dumball= {

   channelName = "unstable";

   modules = [
       ./modules/desktop/sway.nix
       ./modules/stuff.nix
       ./modules/desktop/xorg.nix
   ];};
   hosts.mantaray.modules = [
     ./hosts/mantaray
     ./modules/kexec.nix
   ];





 };
}
