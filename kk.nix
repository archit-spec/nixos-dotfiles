{config, lib, pkgs, ... }:
{
services.nginx = {
  enable = true;
  recommendedTlsSettings = true;
  recommendedOptimisation = true;
  recommendedGzipSettings = true;
  recommendedProxySettings = true;

 virtualHosts = {
   "dumball.in" = {
     serverAliases = [ "www.dumball.in" ];
   };

    "server.dumball.in" = {
      locations."/".proxyPass = "http://localhost:8000";
    };

   # "mumble.mydomain.me" = {
   #   locations."/".proxyPass = "http://localhost:64738";
   # };
  };
};
}
